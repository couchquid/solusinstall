#!/bin/bash

echo "Updating Solus"
sudo eopkg up

echo "Removing unwanted Applications"
sudo eopkg rm -y konversation cantata vlc

echo "Installing alternatives"
sudo eopkg it -y mpv

echo "Installing dev tools"
sudo eopkg it -c system.devel -y

echo "Installing extra packages"
sudo eopkg it -y vim tmux git youtube-dl flatpak xdg-desktop-portal-gtk wine lutris wine-devel wine-32bit-devel winetricks obs-studio gimp inkscape kdenlive vscode

echo "Installing Flathub"
sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

echo "Installing Flatpak Apps"
flatpak install -y flathub \
    com.discordapp.Discord \
    com.slack.Slack \
    com.spotify.Client \
    org.gnome.Builder \
    org.gnome.Fractal \
    org.gnome.Geary \
    org.gnome.Podcasts \
    org.telegram.desktop

while true; do
    read -r -p "Do you wish to install Steam? (y/n) " yn
    case $yn in
        [Yy]* ) sudo eopkg it steam; break;;
        [Nn]* ) exit;;
        * ) echo "Please answer yes or no.";;
    esac
done

while true; do
    read -r -p "Do you wish to install qemu? (y/n) " yn
    case $yn in
        [Yy]* ) sudo eopkg it -y qemu virt-manager; break;;
        [Nn]* ) exit;;
        * ) echo "Please answer yes or no.";;
    esac
done

while true; do
    read -r -p "Do you wish to install jack+ardour? (y/n) " yn
    case $yn in
        [Yy]* ) sudo eopkg it -y jack-audio-connection-kit pulseaudio-module-jack ardour qjackctl calf-plugins; break;;
        [Nn]* ) exit;;
        * ) echo "Please answer yes or no.";;
    esac
done

#set mouse pointer size
#set 24h clock
#set caps as esc

echo "Set ZSH as shell"
sudo usermod -s /bin/zsh couchquid
git clone https://github.com/robbyrussell/oh-my-zsh.git ~/.oh-my-zsh

echo "Install dotfiles"
git clone --bare "https://github.com/couchquid/dotfiles" "$HOME/.dotfiles"
git --git-dir="$HOME/.dotfiles/" --work-tree="$HOME" checkout
git --git-dir="$HOME/.dotfiles/" --work-tree="$HOME" config --local status.showUntrackedFiles no

echo "Install vimrc"
git clone https://github.com/couchquid/vim.git ~/.vim
ln -s ~/.vim/vimrc ~/.vimrc
vim -E +'PlugInstall --sync' +qa
timeout 2s vim

echo "Install Rust"
curl https://sh.rustup.rs -sSf | sh -s -- --default-toolchain stable -y

echo "Install Ripgrep"
~/.cargo/bin/cargo install ripgrep

echo "Install RLS"
~/.cargo/bin/rustup component add rls-preview rust-analysis rust-src --toolchain stable
